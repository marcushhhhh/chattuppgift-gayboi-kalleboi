﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;

namespace shat3
{
    class Program
    {
        static void Main(string[] args)
        {

            message[] messages;
            WebClient wc = new WebClient();
            string json = wc.DownloadString("http://localhost:3000/getinfo");
            Console.WriteLine(json);

            string name = Console.ReadLine();
            json = wc.DownloadString("http://localhost:3000/setinfo?name=" + name);

            Console.WriteLine(json);
            messages = JsonConvert.DeserializeObject<message[]>(json);
            Console.WriteLine(messages[1].text);

        }
    }
    public class message {
        public string name;
        public string date;
        public string text;
    }
}
