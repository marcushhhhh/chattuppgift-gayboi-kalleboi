var express = require('express');
var router = express.Router();
const fetch = require("node-fetch");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/clock', function(req, res, next) {
  let date = new Date();

  res.set("refresh", "1;");

  res.send(date);
});

router.get('/rik', function(req, res, next) {
    fetch("http://data.riksdagen.se/personlista/?parti=M&utformat=json")
        .then(function(d){ return d.json()})
        .then(
            function(d)
            {
                let namn = [];
                let föddår = [];
                let bild = "";

              d.personlista.person.forEach(
                  function(p) {
                      // console.dir(p);
                      namn.push( `${p.efternamn} <br>` );
                      bild += `<img src="${p.bild_url_max}">`;
                      föddår.push(p.fodd_ar);
                  }
              );

              let antal = föddår.length;
              let summa = 0;
              föddår.forEach(
                  function(d) {
                      summa += 1*d;
                  }
              );

                namn = namn.sort();


                let medel = summa/antal;

              let år = new Date();
              år = år.getFullYear();

              let ålder = år - medel;

              res.send("Hello " + ålder);
            });
});

router.get('/fetch', function(req, res, next) {

  res.send(`
  <p class="time">Hello</p>
  <script>
  function readTime() {
    fetch("http://localhost:3000/clock")
        .then(
            function(data) {
                return data.text();
            })
        .then(
            function(data) {
                console.dir(data);
                
                data = data.slice(1, data.length-2);
                
                date = new Date(data);
                
                let timeStr = date.getHours() + " " + date.getMinutes() + " " + date.getSeconds(); 
                
                document.getElementsByClassName("time")[0].innerHTML = timeStr;
            });
    
    setTimeout(readTime, 1000);
  }
  readTime();
  </script>
`);
});


module.exports = router;
