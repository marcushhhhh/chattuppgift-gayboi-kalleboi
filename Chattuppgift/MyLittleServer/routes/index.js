var express = require('express');
var router = express.Router();
const fetch = require("node-fetch");

let highscores = {
    scores: [
        {name: "Soltorg", score: 10},
        {name: "Soltorg", score: 9},
        {name: "Soltorg", score: 8},
        {name: "Soltorg", score: 7},
        {name: "Soltorg", score: 6}

    ]
};

let accounts = {
    account: [
        {
            accountId: 1,
            name: "Marcus",
            age: 18,
            city: "Borlänge"
        },
        {
            accountId: 2,
            name: "Luke",
            age: 23,
            city: "Stockholm"
        }
    ]
};

let chatlog = {
    message: [
        {
            accountId: 1,
            date: "2018-11-16",
            time: "14:48:50",
            message: ""
        },
        {
            accountId: 2,
            date: "2018-11-16",
            time: "14:48:50",
            message: ""
        }
    ]
};

function createAccount() {

}
function Person(name, age, city) {
    this.name = name;
    this.age = age;
    this.city = city;
}



router.get("/chatlog", function (req, res, next) {
    res.send(JSON.stringify(chatlog))
});

router.get("/getscore", function (req, res, next) {
    res.send(JSON.stringify(highscores))
});

router.get("/setscore", function (req, res, next) {
    let highscoreName = req.query.name;
    let highscore = parseInt(req.query.score);

    if (highscore > highscores.scores[4].score) {
        highscores.scores[4].score = highscore;
        highscores.scores[4].name = highscoreName;

        highscores.scores.sort((a, b) => b.score - a.score);
    }

    res.send(JSON.stringify(highscores))
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/clock', function(req, res, next) {
  let date = new Date();

  res.set("refresh", "1;");

  res.send(date);
});

router.get('/rik', function(req, res, next) {
    fetch("http://data.riksdagen.se/personlista/?iid=&fnamn=&enamn=&f_ar=&kn=&parti=&valkrets=&rdlstatus=samtliga&org=&utformat=json&termlista=")
        .then(function(d) {
            return d.json();
    })
      .then(function(d) {
          d.personlista.person.forEach(function (p) {
              console.dir(p);
          });
      });

    res.send("Hämtat");
});

router.get('/fetch', function(req, res, next) {

  res.send(`
  <p class="time">Hello</p>
  <script>
  function readTime() {
    fetch("http://localhost:3000/clock")
        .then(
            function(data) {
                return data.text();
            })
        .then(
            function(data) {
                console.dir(data);
                
                data = data.slice(1, data.length-2);
                
                date = new Date(data);
                
                let timeStr = date.getHours() + " " + date.getMinutes() + " " + date.getSeconds(); 
                
                document.getElementsByClassName("time")[0].innerHTML = timeStr;
            });
    
    setTimeout(readTime, 1000);
  }
  readTime();
  </script>
`);
});


module.exports = router;
